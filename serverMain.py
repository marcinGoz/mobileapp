
from server import Server
from messageHandler import MessageHandler
import logging

def main():
    logging.basicConfig(filename="./runtime.log", level=logging.INFO)
    server = Server()
    server.initServer()
    msgHandler = MessageHandler(server)
    msgHandler.run()

if __name__ == "__main__":
    main()
