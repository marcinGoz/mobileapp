from pyftpdlib.authorizers import DummyAuthorizer
from pyftpdlib.handlers import FTPHandler
from pyftpdlib.servers import FTPServer

import logging

from user import User
from group import Group
from database import Database


class Server:
    def __init__(self):
        self.authorizer = DummyAuthorizer()
        handler = FTPHandler
        handler.authorizer = self.authorizer
        self.server = FTPServer(("127.0.0.1", 21), handler) # replace with an IP of the server
        self.database = Database()
        self.connections = dict()

    def initServer(self):
        users = self.database.getUsers()       

        for user in users:
            self.server.add_user(user.getName(), user.getPassword(), "./", perm="elradfmw")
            self.updatePermForUser(user)

        self.server.serve_forever()

    def registerUser(self, user):
        self.authorizer.add_user(user.getName(), user.getPassword(), "./public", perm="elradfmw")
        pubGroup = Group("public", self.database)
        self.database.addUserToGroup(user, pubGroup)

    def login(self, connId, user):
        if self.database.authenticateUser(user):
            self.connections[connId] = user.getName()
            return True
        
        return False


    def addUserToGroup(self, connId, user, group):
        try:
            adminName = self.connections[connId]
            if not self.database.isUserInGroup(User(adminName), Group("admin")):
                logging.warning("No admin privilages for adding users to groups")
                return False

            self.database.addUserToGroup(user, group)

        except KeyError:
            logging.warning("addUserToGroup: user not logged-in")
            return False

        self.updatePermForUser(user)

    def updatePermForUser(self, user):
        foldersPerms = self.database.getFoldersPermsForUser(user)

        if self.database.isUserInGroup(user, Group("admin")):
           self.authorizer.override_perm(user.getName(), "./", "elradfmw", recursive=True) 
           return
        
        for path in foldersPerms.keys():
           self.authorizer.override_perm(user.getName(), path, foldersPerms[path]) 



