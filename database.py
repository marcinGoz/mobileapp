
import logging
import sqlite3
from user import User
from group import Group



class Database:
    lastGroupId = 1
    def __init__(self):
        self.conn = sqlite3.connect("server.db")
        self.cur = self.conn.cursor()

    def addUser(self, user):
        self.cur.execute("INSERT INTO users VALUES (?,?)", (user.getName(),
            user.getPassword()))
        self.cur.execute("SELECT * FROM groups")
        data = self.cur.fetchone()
        if len(data) == 0:
            self.cur.execute("INSERT INTO groups VALUES (?, public)",
                    (Database.lastGroupId,))
            Database.lastGroupId += 1
            self.cur.execute("INSERT INTO groups VALUES (?, admin)",Database.lastGroupId)
            Database.lastGroupId += 1

    def isUserInGroup(self, user, group):
        self.cur.execute("SELECT * FROM group_users WHERE user='?' AND gr='?'",
                (user.getName(), group.getName()))
        data = self.cur.fetchone()
        if len(data) == 0:
            logging.debug("No user %s in group %s" % (user.getName(),
                group.getName()))
            return False

        return True


    def authenticateUser(self, user):
        self.cur.execute("SELECT password FROM users WHERE name='?'",
                (user.getName(),))
        data = self.cur.fetchone()
        if len(data) == 0:
            logging.info("Incorrect username %s" % (user.getName(),))
            return False
        if data.strip() != user.password():
            logging.info("Incorrect password")
            return False
        return True


    def addUserToGroup(self, user, group):
        grId = self.getGroupId(group.getName())
        self.cur.execute("SELECT * FROM group_users WHERE user='?' AND gr='?'",
                (user.getName(), grId))
        data = self.cur.fetchone()
        if len(data) == 0:
            self.cur.execute("INSERT INTO group_users VALUES (?, ?)",
                    user.getName(), grId)

    def removeUserFromGroup(self, user, group):
        self.cur.execute("UPDATE users SET groups='?' WHERE user='?'",
                (group.getName(), user.getName()))

    def getGroupIdsForUser(self, user):
        self.cur.execute("SELECT group_id FROM group_users WHERE user='?'",
                (user.getName()))
        return self.cur.fetchall()
        

    def getFoldersPermsForUser(self, user):
        groupIds = self.getGroupIdsForUser(user)
        dirPermDict = dict()
        prevDirPermDict = dict()
        
        for grId in groupIds:
            self.cur.execute("SELECT path FROM group_dir WHERE group_id='?'",
                    (grId,))
            dirs = self.cur.fetchall()
            self.cur.execute("SELECT permissions FROM groups WHERE \
                    group_id='?'", (grId,))
            perm = self.cur.fetchone()

            for d in dirs:
                try:
                    if len(perm) > len(prevDirPermDict): # assign stronger permissions - naive solution 
                        dirPermDict[d] = perm
                        prevDirPermDict[d] = perm
                except KeyError:
                    dirPermDict[d] = perm
                    prevDirPermDict[d] = perm

        return dirPermDict
    
    def getUsers(self):
        self.cur.execute("SELECT username, password FROM users")
        users = []
        
        data = self.cur.fetchall()

        for d in data:
            users.append(User(str(d[0], d[1])))


    def getGroupId(self, name):
        self.cur.execute("SELECT group_id FROM groups WHERE name=?",(name,))
        data = self.cur.fetchone()
        #if len(data) == 0:

