
import logging

from user import User
from group import Group
from server import Server

import socket
import thread
import struct
import json
HOST = ''     
PORT = 50001

# msg ids
SIGN_IN_MSG             = 101 #[SIGN_IN_MSG {"username" : "str1", "password" : "str2"}]
LOG_IN_MSG              = 102
ADD_USER_TO_GROUP_MSG   = 103
SET_PERM_ON_GROUP_MSG   = 104

# message format:
# - first send 8-bit lenght of json msg
# - than send json msg in format: [MSG_ID, { param1 : value1, param2 : value2 ... }]
 
class MessageHandler:
    def __init__(self, server):
        self.server = server
        self.lastConnId = 1
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.bind((HOST, PORT))
        self.sock.listen(1)

    def handleConnection(self, conn, connId):
        while True:
            msg = self.receiveMsg(conn, connId)

    def receiveMsg(self, conn, connId):
        data = ""
        msgLen = 0
        hdrLen = 4
        while len(data) < hdrLen:
            temp = conn.recv(hdrLen)
            data += temp
        
        msgLen = struct.unpack("=I", data)
        data = ""
        while len(data) < msgLen:
            temp = conn.recv(msgLen)
            data += temp
        
        self.handleMsg(json.load(data), connId)
    


    def handleMsg(self, data, connId):
        msgId = data[0]
        payload = data[1]
        if msgId == SIGN_IN_MSG:
            newUser = User(payload["username"], payload["password"])
            self.server.registerUser(newUser)
        elif msgId == LOG_IN_MSG:
            usr = User(payload["username"], payload["password"])
            self.server.login(connId, usr)

        elif msgId == ADD_USER_TO_GROUP_MSG:
            usr = User(payload["username"])
            grp = Group(payload["group"])
            self.server.addUserToGroup(connId, usr, grp)

        else:
            logging.warning("Unrecognized message")
        
    def run(self):
        while True:
            conn, addr = self.sock.accept()
            thread.start_new_thread(self.handleConnection, (conn,self.lastConnId))
            self.lastConnId += 1


